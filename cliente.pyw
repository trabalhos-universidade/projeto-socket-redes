#!/usr/bin/python

import gtk
import gtk.glade
from time import sleep
import os
import socket

class PPT:
    # Classe que cuida do envio e conexao 
    def __init__(self):
        self.sock=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.cont = 0
        self.regras = {"Pedra X Pedra" : "Empate", "Pedra X Papel" : "Voce perdeu!", "Pedra X Tesoura" : "Voce venceu! - Pedra amassa ou quebra tesoura", "Papel X Papel" : "Empate", "Papel X Pedra" : "Voce venceu! - Papel embrulha pedra", "Papel X Tesoura" : "Voce perdeu!", "Tesoura X Tesoura" : "Empate", "Tesoura X Pedra" : "Voce perdeu!", "Tesoura X Papel" : "Voce venceu! - Tesoura corta papel"}
		
    def con(self, ip, port=50007):
        self.sock.connect((ip, port))

    def waitCon(self, ip='', port=50007, l=1):
        self.sock.bind((ip, port))
        self.sock.listen(l)
        (clientsocket, address) = self.sock.accept()
        self.sock = clientsocket

    def send(self, msg):
        sent = self.sock.send(msg)
        if sent == 0:
            raise BrokenCon, "socket connection broken"

    def receive(self, bytes):
        return self.sock.recv(bytes)

    def close(self):
        self.sock.close()
		
    def contar(self):
        self.cont = self.cont + 1

jogo = PPT()      
gladefile = gtk.glade.XML('cliente.glade')
ip = gladefile.get_widget('ip')
campo_ip = gladefile.get_widget('campo_ip')
conectar = gladefile.get_widget('bConectar')
resultado_status = gladefile.get_widget('resultado_status')

def on_bPedra_pressed(*args):
    resultado_status.set_text("Cliente escolheu a opcao Pedra.")
    data = jogo.receive(1024)	    
    jogo.send("Pedra")
     
def on_bPapel_pressed(*args):      
    resultado_status.set_text("Cliente escolheu a opcao Papel.")
    data = jogo.receive(1024)	    
    jogo.send("Papel")
	
def on_bTesoura_pressed(*args):    
    resultado_status.set_text("Cliente escolheu a opcao Tesoura.")
    data = jogo.receive(1024)    
    jogo.send("Tesoura")

def on_bConectar_pressed(*args):      
   resultado_status.set_text("Conexao com o servidor realizado com sucesso!")
   jogo.con(campo_ip.get_text())   
   jogo.send(str(jogo.cont))

def resultado(*args):
   data = jogo.receive(1024)   
   resultado_status.set_text(jogo.regras[data])      

dic = { 
          "gtk_main_quit" : gtk.main_quit, 
          "on_bConectar_pressed" : on_bConectar_pressed,
		  "on_bPedra_pressed" : on_bPedra_pressed,
		  "on_bPapel_pressed" : on_bPapel_pressed,
		  "on_bTesoura_pressed" : on_bTesoura_pressed,
          "on_bResultado_pressed" : resultado		  
      }

gladefile.signal_autoconnect(dic)

gtk.main()

