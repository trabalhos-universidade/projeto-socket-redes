import gtk
import gtk.glade
from time import sleep
import os
import socket

class PPT:
    # Classe que cuida do envio e conexao 
    
    def __init__(self):
        self.sock=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.cont = 0
        self.clic_client = ""
        self.jogada = {}
        self.regras = {"Pedra X Pedra" : "Empate", "Pedra X Papel" : "Voce perdeu!", "Pedra X Tesoura" : "Voce venceu! - Pedra amassa ou quebra tesoura", "Papel X Papel" : "Empate", "Papel X Pedra" : "Voce venceu! - Papel embrulha pedra", "Papel X Tesoura" : "Voce perdeu!", "Tesoura X Tesoura" : "Empate", "Tesoura X Pedra" : "Voce perdeu!", "Tesoura X Papel" : "Voce venceu! - Tesoura corta papel"}

    def con(self, ip, port=50007):
        self.sock.connect((ip, port))

    def waitCon(self, ip='', port=50007, l=1):
        self.sock.bind((ip, port))
        self.sock.listen(l)
        (clientsocket, address) = self.sock.accept()
        self.sock = clientsocket        

    def send(self, msg):
        sent = self.sock.send(msg)
        if sent == 0:
            raise BrokenCon, "socket connection broken"

    def receive(self, bytes):
        return self.sock.recv(bytes)

    def close(self):
        self.sock.close()

    def contar(self):
        self.cont = self.cont + 1
		
jogo = PPT()      

gladefile = gtk.glade.XML('servidor.glade')


# ----------------------------------------
# widgets
# ----------------------------------------

conectar = gladefile.get_widget('bConectar')
resultado_status = gladefile.get_widget('resultado_status')

def msg_conexao_cliente(*args):
    resultado_status.set_text("Aguardando conexao do cliente!")    

def on_bConectar_pressed(*args):   
    jogo.jogada = {}
    jogo.waitCon()
    resultado_status.set_text("Conexao com o cliente realizada com sucesso!")    
    jogo.clic_client = jogo.receive(1024)

def on_bPedra_pressed(*args):
    resultado_status.set_text("Servidor escolheu a opcao Pedra.")
    if jogo.clic_client == "0":
       jogo.jogada[0] = "Pedra"
       jogo.send("Servidor realizou jogada.")
    	
def on_bPapel_pressed(*args):  
     resultado_status.set_text("Servidor escolheu a opcao Papel.")
     if jogo.clic_client == "0":
       jogo.jogada[0] = "Papel"
       jogo.send("Servidor realizou jogada.")       

def on_bTesoura_pressed(*args):
     resultado_status.set_text("Servidor escolheu a opcao Tesoura.")
     if jogo.clic_client == "0":
       jogo.jogada[0] = "Tesoura"
       jogo.send("Servidor realizou jogada.")       

def resultado(*args):
    data = jogo.receive(1024)
    jogo.jogada[1] = data    
    a = jogo.jogada[0]+" X "+jogo.jogada[1]
    jogo.send(jogo.jogada[1]+" X "+jogo.jogada[0])
    resultado_status.set_text(jogo.regras[a])
    jogo.jogada = {}
    
dic = { 
          "gtk_main_quit" : gtk.main_quit, 
		  "on_bConectar_clicked" : msg_conexao_cliente,
          "on_bConectar_pressed" : on_bConectar_pressed,		  
		  "on_bPedra_pressed" : on_bPedra_pressed,
		  "on_bPapel_pressed" : on_bPapel_pressed,
		  "on_bTesoura_pressed" : on_bTesoura_pressed, 
		  "on_bResultado_pressed" : resultado		  
      }

gladefile.signal_autoconnect(dic)

gtk.main()
